app.service('IO', function (CONFIG)
{
    this.socket = io(CONFIG.api);

    return this.socket;
});