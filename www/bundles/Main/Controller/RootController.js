app.controller('RootController', function($rootScope, $scope, $ionicLoading)
{
    function init()
    {

    }


    /* ================================================= *
     * Public Functions
     * ================================================= */

    $rootScope.loadingUntilResolved = function(resolvedObject)
    {
        $ionicLoading.show({
            noBackdrop: true,
            templateUrl: 'bundles/Main/templates/_loading.html'
        });

        resolvedObject.$promise.then(function() {
            $ionicLoading.hide();
        })
    }


    /* ================================================= *
     * Private Functions
     * ================================================= */




    /* ================================================= *
     * Init
     * ================================================= */
    init();

});