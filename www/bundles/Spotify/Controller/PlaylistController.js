app.controller('SpotifyPlaylistController', function($rootScope, $scope, $stateParams, $state, Playlist)
{
    function init()
    {
        var uri = $stateParams.uri;

        $scope.playlist = Playlist.get({uri: uri});

        $scope.loadingUntilResolved($scope.playlist);

        //$scope.playlist = _.find($scope.playlists, function(p) {
        //    return p.uri == uri;
        //});
    }


    /* ================================================= *
     * Public Functions
     * ================================================= */




    /* ================================================= *
     * Private Functions
     * ================================================= */




    /* ================================================= *
     * Init
     * ================================================= */
    init();

});