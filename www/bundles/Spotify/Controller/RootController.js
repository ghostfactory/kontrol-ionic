app.controller('SpotifyRootController', function($rootScope, $scope, CONFIG, SPOTIFY, SpotifyService, IO)
{
    function init()
    {
        initNowPlaying();
        initEvents();
    }

    function initNowPlaying()
    {
        $rootScope.nowPlaying = null;
        SpotifyService.fetchCurrentTrack().success(function(track) {
            $rootScope.nowPlaying = track;
        }).error(function(error) {

        });
    }

    function initEvents()
    {
        IO.on(SPOTIFY.events.playbackStarted, function(track) {
            $rootScope.nowPlaying = track;
            $scope.$apply();
        });

        IO.on(SPOTIFY.events.playbackEnded, function(track) {
            $rootScope.nowPlaying = null;
            $scope.$apply();
        });
    }


    /* ================================================= *
     * Public Functions
     * ================================================= */




    /* ================================================= *
     * Private Functions
     * ================================================= */




    /* ================================================= *
     * Init
     * ================================================= */
    init();

});