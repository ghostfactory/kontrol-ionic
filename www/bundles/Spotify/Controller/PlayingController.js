app.controller('SpotifyPlayingController', function($rootScope, $scope, $stateParams, $state, $http, $interval, CONFIG, SPOTIFY, IO, SpotifyService)
{
    function init()
    {
        $scope.progressInterval = undefined;
        //$scope.positionPercent = 0;

        initPlayTrack();
        startProgress();
        initEvents();
    }

    function initPlayTrack()
    {
        var uri = $stateParams.uri;
        var playlistUri = $stateParams.playlistUri;
        var trackUri = $stateParams.trackUri;

        // Not trying to play a track
        if(!uri && !playlistUri && !trackUri){
            return;
        }

        // Play track
        var playPromise = uri ? SpotifyService.playTrack(uri) : SpotifyService.playTrack(playlistUri, trackUri);
        playPromise.success(function(track) {
            //console.log(track);
            //// NOTE: Setting new nowPlaying is handled by IO event.
            //$scope.positionPercent = SpotifyService.positionPercent(track);
        }).error(function(error) {

        });
    }

    function startProgress()
    {
        // Make sure to stop any current progress interval
        cancelProgress();

        $scope.progressInterval = $interval(function () {
            SpotifyService.fetchCurrentTimePosition().success(function (timePosition) {
                $scope.nowPlaying.timePosition = timePosition;
                $scope.nowPlaying.timePercent = SpotifyService.positionPercent($scope.nowPlaying);
            });
        }, SPOTIFY.progressInterval);

        //$scope.progressInterval = $interval(function () {
        //    $scope.nowPlaying.timePosition += SPOTIFY.progressInterval;
        //    $scope.nowPlaying.timePercent = SpotifyService.positionPercent($scope.nowPlaying);
        //
        //    console.log("HIT");
        //    console.log($scope.nowPlaying);
        //}, SPOTIFY.progressInterval);
    }

    function cancelProgress()
    {
        $interval.cancel($scope.progressInterval);
        $scope.progressInterval = undefined;
    }

    function initEvents()
    {
        // Start progress

        IO.on(SPOTIFY.events.playbackStarted, function(track) {
            console.log("Playback Started");

            //SpotifyService.fetchAlbumArtwork(track, function(albumArtwork) {
            //    $scope.nowPlaying.album.artwork = albumArtwork;
            //});

            startProgress();
        });

        IO.on(SPOTIFY.events.playbackResumed, function(track) {
            console.log("Playback Resumed");
            startProgress();
        });

        // Stop progress

        IO.on(SPOTIFY.events.playbackPaused, function(track) {
            console.log("Playback Paused");
            cancelProgress();
            $scope.$apply();
        });

        IO.on(SPOTIFY.events.playbackEnded, function(track) {
            console.log("Playback Ended");
            cancelProgress();
            $scope.$apply();
        });

        $scope.$on('$destroy', function() {
            cancelProgress();
            if($scope.nowPlaying) {
                $scope.nowPlaying.album.artwork = null;
            }
        });
    }


    /* ================================================= *
     * Public Functions
     * ================================================= */

    $scope.resume = function()
    {
        SpotifyService.resume();
    }

    $scope.pause = function()
    {
        SpotifyService.pause();
    }

    $scope.stop = function()
    {
        SpotifyService.stop();
    }


    /* ================================================= *
     * Private Functions
     * ================================================= */




    /* ================================================= *
     * Init
     * ================================================= */
    init();

});