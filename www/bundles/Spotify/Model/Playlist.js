app.factory('Playlist', function($resource, CONFIG)
{
    return $resource(CONFIG.api + 'mopidy/playlists/:uri', { uri: '@uri' }, {
        update: {
            method: 'PUT'
        }
    });
});