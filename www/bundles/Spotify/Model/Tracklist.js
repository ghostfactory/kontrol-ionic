app.factory('Tracklist', function($resource, CONFIG)
{
    return $resource(CONFIG.api + 'mopidy/tracklist', { uri: '@uri' }, {
        update: {
            method: 'PUT'
        }
    });
});