app.constant('SPOTIFY', {
    progressInterval: 1000,
    events: {
        playbackStateChanged: "mopidy.playbackStateChanged",
        playbackStarted: "mopidy.playbackStarted",
        playbackPaused: "mopidy.playbackPaused",
        playbackResumed: "mopidy.playbackResumed",
        playbackEnded: "mopidy.playbackEnded",
        tracklistChanged: "mopidy.tracklistChanged"
    }
});